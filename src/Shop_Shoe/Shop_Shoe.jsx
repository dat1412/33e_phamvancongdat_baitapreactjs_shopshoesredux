import React, { Component } from "react";
import { connect } from "react-redux";
import Cart_Shoe from "./Cart_Shoe";
import Item_Shoe from "./Item_Shoe";
import {
  ADD_TO_CART,
  CHANGE_QUANTITY,
} from "./redux/constants/shopShoeConstant";

class Shop_Shoe extends Component {
  renderContent = () => {
    return this.props.listShoe.map((item, index) => {
      return (
        <Item_Shoe
          key={index}
          dataShoe={item}
          handleAddToCart={this.props.handleAddToCart}
        />
      );
    });
  };
  render() {
    return (
      <div className="container py-5">
        <Cart_Shoe
          cart={this.props.cart}
          handleChangeQuantity={this.props.handleChangeQuantity}
        />
        <div className="row">{this.renderContent()}</div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    listShoe: state.shopShoeReducer.shoe,
    cart: state.shopShoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (shoe) => {
      dispatch({
        type: ADD_TO_CART,
        payload: shoe,
      });
    },

    handleChangeQuantity: (id, value) => {
      dispatch({
        type: CHANGE_QUANTITY,
        payload: {
          id: id,
          value: value,
        },
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Shop_Shoe);

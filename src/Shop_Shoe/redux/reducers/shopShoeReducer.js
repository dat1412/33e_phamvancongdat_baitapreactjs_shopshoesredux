import { data_shoes } from "../../data_shoes";
import { ADD_TO_CART, CHANGE_QUANTITY } from "../constants/shopShoeConstant";

let initialState = {
  shoe: data_shoes,
  cart: [],
};

export const shopShoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let index = state.cart.findIndex((item) => {
        return item.id === payload.id;
      });
      let cloneCart = [...state.cart];

      if (index === -1) {
        let newSp = { ...payload, quantity: 1 };
        cloneCart.push(newSp);
      } else {
        cloneCart[index].quantity++;
      }

      state.cart = cloneCart;

      return { ...state };
    }
    case CHANGE_QUANTITY: {
      let index = state.cart.findIndex((item) => {
        return item.id === payload.id;
      });
      if (index === -1) {
        return;
      }

      let cloneCart = [...state.cart];
      cloneCart[index].quantity += payload.value;

      cloneCart[index].quantity === 0 && cloneCart.splice(index, 1);

      state.cart = cloneCart;

      return { ...state };
    }
    default:
      return state;
  }
};

import { combineReducers } from "redux";
import { shopShoeReducer } from "./shopShoeReducer";

export let rootReducer_shopShoe = combineReducers({
  shopShoeReducer,
});
